import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import mainStore from './store/store';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <Provider store={mainStore}>
        <App />
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
