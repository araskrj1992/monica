import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from "react-router-dom";
import OrderItemList from './monica/OrderItem/OrderItemList'


class App extends Component {

  render() {

    return (
      <Router>
        <Switch>
          <Route exact path="/" component={OrderItemList} />
        </Switch>
      </Router>
    );
  }
}

export default App;
