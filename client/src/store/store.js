import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

// Logger with default options 
import { createLogger } from 'redux-logger'
import mainReducer from '../reducer';


const logger = createLogger({
    predicate: (getState, action) => action.type !== undefined
});

// let initialState = { login: false };


const mainStore = createStore(
    mainReducer,
    applyMiddleware(logger, thunk)
);

export default mainStore;
