import React from 'react'
import TextInput from '../TextInput'
const OrderItem = () => {
    return (
        <div className="item_wrapper">
            <div className="item_input_wrapper">
                <TextInput name="item_number" label="item number" htmlId="item_number" classes="item_input number" />
                <TextInput name="qty" label="qty" htmlId="item_number" classes="item_input qty" />
                <TextInput name="comments" label="comments" htmlId="item_number" classes="item_input comments" />
                <TextInput name="cost" label="cost center" htmlId="item_number" classes="item_input cost" />
                <button className="item_btn">Service</button>
            </div>
            <div className="item_info_wrapper">
                <div className="item_info">
                    <img src="/images/img_block.png" alt="blank img block" />
                </div>
                <div className="item_info description">
                    <label>item description</label>
                    <p>-</p>
                </div>
                <div className="item_info price">
                    <label>price</label>
                    <p>-</p>
                </div>
                <div className="btn_wrapper">
                    <button className="item_btn">Remove</button>
                </div>
            </div>
        </div>
    )
}

export default OrderItem;