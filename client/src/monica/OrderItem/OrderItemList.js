import React, { Component } from 'react';
import OrderItem from './OrderItem'
import './OrderItem.scss'
class OrderItemContainer extends Component {

    render() {

        return (
            <section className="item_list_wrapper">
                <OrderItem />
                <OrderItem />
                <OrderItem />
                <OrderItem />
                <OrderItem />
                <OrderItem />
                <OrderItem />
                <OrderItem />

                <div className="list_btn_wrapper">
                    <button className="item_btn">+ Enter More Items</button>
                    <div className="right">
                        <button className="item_btn">Add To List</button>
                        <button className="item_btn add_to_cart">Add To Cart</button>
                    </div>
                </div>
            </section>
        )
    }
}

export default OrderItemContainer;