import React from 'react';
import PropTypes from 'prop-types';

/** Label with required field display, htmlFor, and block styling */
function Label({
  htmlFor, label, required, classes
}) {
  return (
    <label className={classes} htmlFor={htmlFor}>
      {label} { required && <span className="label-required"> *</span> }
    </label>
  );
}

Label.propTypes = {
  /** HTML ID for associated input */
  htmlFor: PropTypes.string.isRequired,

  /** Label text */
  label: PropTypes.string.isRequired,

  /** Display asterisk after label if true */
  required: PropTypes.bool,

  /** List of classes to apply to the label */
  classes: PropTypes.string
};

Label.defaultProps = {
  required: false,
  classes: ''
};

export default Label;

