import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Label from '../Label';

/** Text input with integrated label to enforce consistency in layout, error display, label placement, and required field marker. */
export default class TextInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value
    };
    this.onChange = this.onChange.bind(this);
  }

  onChange(ev) {
    this.setState({
      value: ev.target.value
    });
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(ev);
    }
  }
  render() {
    const {
      htmlId, name, label, type, classes, required, placeholder, error, children
    } = this.props;
    return (
      <div className={`textinput od-components-th ${classes}`}>
        <Label
          htmlFor={htmlId} label={label} classes="label"
          required={required}
        />
        <input
          id={htmlId}
          type={type || 'text'}
          name={name}
          placeholder={placeholder}
          value={this.state.value}
          onChange={this.onChange}
          style={error && { border: 'textinput-state-error' }}
        />
        {children}
        {error && <div className="textinput-error">{error}</div>}
      </div>
    );
  }
}

TextInput.propTypes = {
  /** Unique HTML ID. Used for tying label to HTML input. Handy hook for automated testing. */
  htmlId: PropTypes.string.isRequired,

  /** Input name. Recommend setting this to match object's property so a single change handler can be used. */
  name: PropTypes.string.isRequired,

  /** Input label */
  label: PropTypes.string.isRequired,

  /** Input type */
  type: PropTypes.oneOf(['text', 'number', 'password']),

  /** Mark label with asterisk if set to true */
  required: PropTypes.bool,

  /** Classes to style the element */
  classes: PropTypes.string,

  /** Function to call onChange */
  onChange: PropTypes.func.isRequired,

  /** Placeholder to display when empty */
  placeholder: PropTypes.string,

  /** Value */
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool
  ]),

  /** String to display when error occurs */
  error: PropTypes.string,

  /** Child component to display next to the input */
  children: PropTypes.node
};

TextInput.defaultProps = {
  required: false,
  type: 'text',
  classes: '',
  value: '',
  placeholder: '',
  error: null,
  children: null
};

