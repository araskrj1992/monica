const express = require('express');
const path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser')
var request = require('request')
const app = express();
// var api = require('./api')

// Serve static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));


app.use(bodyParser.json())
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }))


app.use(function (req, res, next) {
  var contentType = req.headers['content-type'];
  if (req.method === 'POST' && (!contentType || contentType.indexOf('application/json') === -1)) {
    return res.send(400);
  }
  next();
});

// app.use('/api', api);

// The "catchall" handler: for any request that doesn't
// match one above, send back React's index.html file.
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/client/build/index.html'));
});

const port = process.env.PORT || 5000;
app.listen(port);

console.log(`Password generator listening on ${port}`);
